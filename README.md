# Pythron

Pythron is a framework which allows to develop applications in `eel` (https://github.com/samuelhwilliams/Eel) and getting an executable running in a desktop window.


List of files:
* `app.py` : script to edit for creating the app
* `main.py` : main script which is compiled by `compile.py`
* `compile.py` : script which needs to be run for creating the package
* `[web]` : [folder] which contains the gui static files rendered by `app.py`, such as `main.html`, js files and css files (linked in the html files)


## How to develop
1. Edit `app.py` in order to develop your applications, writing python functions
    > Only edit the code within the block `# ~~~ Change your code here ~~~`
1. Edit `web/main.html` in order to create your GUI. Since Pythron includes `eel`, the html and js files can use python functions exposed, using the syntax explained in eel module.
1. Compile you application, in order to obtain the executable in the `dist/` directory.
    ```
    python3 compile.py
    ```
