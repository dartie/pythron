from app import main as main_app
from pyside_gui import main as main_gui
import os
import sys
import threading
import traceback
from input_args import check_args
import PyInstaller


def main(args=None):
    if args is None:
        args = check_args()
    
    # print('Port: {}'.format(args.pythron_port))

    ta = threading.Thread(target=main_app, args=(args,))
    tb = threading.Thread(target=main_gui, args=(args,))
    ta.start()
    tb.start()

    tb.join()
    ta.join()

    print('Bye')

    # sys.exit(0)
    # main_app()
    # main_gui()


if __name__ == '__main__':
    try:
        main(args=None)
    except:
        traceback.print_exc()
"""        
    except KeyboardInterrupt:
        print('\n\nBye!')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
"""

# TODO: add Qt window top menu
# TODO: submit with eel
# TODO: taskbar icon, window icon