from argparse import ArgumentParser, RawTextHelpFormatter, SUPPRESS
# import Namespace
try:
    from types import SimpleNamespace as Namespace  # available from Python 3.3
except ImportError:
    class Namespace:
        def __init__(self, **kwargs):
            self.__dict__.update(kwargs)


def check_args():
    parser = ArgumentParser(formatter_class=RawTextHelpFormatter, description="""
    Description

    """)

    # port number which needs to be used for running eel bottle server.
    # !! IT MUST NOT BE REMOVED !!
    parser.add_argument("--port", dest="pythron_port", type=int, default=1195, help=SUPPRESS)

    # Options
    parser.add_argument("-debug", dest="debug", action='store_true', help=SUPPRESS)
                        # help='Increase output verbosity. Output files don\'t overwrite original ones'

    parser.add_argument("-op", "--output", dest="output_path",
                        help="output path. it has given in input to this application")
                        # action='store_true'
                        # nargs="+",
                        # nargs='?',  # optional argument
                        # default=""
                        # type=int
                        # choices=[]

    #parser.add_argument("source_file", help="source file must be analyzed")

    # return empty Namespace if you don't need input arguments or the arguments retrieved
    args = parser.parse_args()  # it returns input as variables (args.dest)
    # args = Namespace()

    # end check args

    return args