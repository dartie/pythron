import eel
import sys
import os


@eel.expose                         # Expose this function to Javascript
def say_hello_py(x):
    print('Hello from %s' % x)


def main(args):
    # Set web files folder and optionally specify which file types to check for eel.expose()
    #   *Default allowed_extensions are: ['.js', '.html', '.txt', '.htm', '.xhtml']
    eel.init('web', allowed_extensions=['.js', '.html'])

    try:
        # eel.start('main.html')
        eel.start('main.html', port=args.pythron_port, host='localhost', mode=None)  # , block=None)# Start (this blocks and enters loop)
    except (SystemExit, MemoryError, KeyboardInterrupt):
        # We can do something here if needed
        # But if we don't catch these safely, the script will crash
        pass

    # ~~~ Change your code here ~~~
    say_hello_py('Python World!')
    eel.say_hello_js('Python World!')  # Call a Javascript function

    # ~~~ /Change your code here ~~~


if __name__ == '__main__':
    try:
        main(None)
    except KeyboardInterrupt:
        print('\n\nBye!')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
