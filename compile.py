from subprocess import Popen, PIPE
import subprocess
import shlex
import site
import os
import sys
from argparse import ArgumentParser, RawTextHelpFormatter


def myrun(cmd):
    """
    from http://blog.kagesenshi.org/2008/02/teeing-python-subprocesspopen-output.html
    """
    p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    stdout = []
    while True:
        line = p.stdout.readline()
        line = line.strip().decode('utf-8')  # strip could be replaced by a remove suffix
        stdout.append(line)
        print(line)
        if line == '' and p.poll() != None:
            break
    return '\n'.join(stdout)


def main():
    parser = ArgumentParser(formatter_class=RawTextHelpFormatter, description="""
    Compiles the pythron application

    """)

    # Options
    parser.add_argument("--noconsole", dest="noconsole", action='store_true', help="If enabled, it hides the console window when running the executable")

    args = parser.parse_args()  # it returns input as variables (args.dest)

    if args.noconsole:
        args.noconsole = '--noconsole'
    else:
        args.noconsole = ''

    # end check args

    if os.sep == '/':
        # unix
        print('Unix')
        usersitepackages = site.getusersitepackages()
        var_sep = ':'
        eeljs_fullpath = os.path.join(usersitepackages, 'eel', 'eel.js')
    else:
        # windows
        print('Windows')
        usersitepackages = os.path.split(sys.executable)[0]
        var_sep = ';'
        eeljs_fullpath = os.path.join(usersitepackages, 'Lib', 'site-packages', 'eel', 'eel.js')

    cmd = 'pyinstaller main.py --hidden-import bottle_websocket --add-data {eeljs_fullpath}{var_sep}eel --add-data web{var_sep}web --clean --onefile {noconsole}'.format(eeljs_fullpath=eeljs_fullpath, var_sep=var_sep, noconsole=args.noconsole)
    print(cmd)
    myrun(cmd)
    # pyinstaller main.py --hidden-import bottle_websocket --add-data /home/dartie/.local/lib/python3.7/site-packages/eel/eel.js:eel --add-data web:web --clean --onefile


if __name__ == "__main__":
    main()
